# My_sql-note

查看路径：SQL> select * from v$logfile;

SQL>select * from v$sql (#查看最近所作的操作)

SQL>select * from v$sqlarea（#查看最近所作的操作）

1、概述
作用:通过使用包DBMS_LOGMNR和DBMS_LOGMNR_D,可以分析重做日志和归档日志所记载的事务变化,最终确定误操作(例如DROP TABLE)的时间,跟踪用户事务操作,跟踪并还原表的DML操作.

回到顶部

2、包的组成

1）、dbms_logmnr.add_logfile
作用：用于为日志分析列表增加或删除日志文件，或者建立日志分析列表。
语法：dbms_logmnr.add_logfile(LogFileName in varchar2,Option in binary_integer default addfile);
其中LogFileName指定要增加或删除的日志文件名称，Option指定选项(dbms_logmnr.new建立日志分析列表，dbms_logmnr.addfile增加日志文件，dbms_logmnr.removefile删除文件)

2）、dbms_logmnr.start_logmnr
作用：用于启动logmnr会话
语法：dbms_logmnr.start_logmnr(startscn in number default o,endscn in number default 0,
starttime in date default '01-jan-1988',endtime in date default '01-jan-2988',
dictfilename in varchar2 default '',option in binary_integer default 0);
其中startscn指定日志分析的起始scn值，endscn指定日志分析的结束scn值，starttime指定日志分析的起始时间，endtime指定日志分析的结束时间，
dictfilename指定日志分析要使用的字典文件名，option指定logminer分析选项。

3）、dbms_logmnr.end_logmnr
作用：结束logminer会话
语法：dbms_logmnr.end_logmnr

4）、dbms_logmnr.mine_value
作用：用于返回要摘取的列信息，该函数在启动logminer之后调用。
语法：dbms_logmner.mine_value(sql_redo_undo in raw,column_name in varchar2 default '') return varchar2;
其中sql_redo_undo用于指定要摘取的数据(redo_value或undo_value)，column_name用于指定要摘取的列(格式：schema.table.column);

5）、dbms_logmnr.column_present
作用：用于确定列是否出现在数据的redo部分或undo部分
语法：dbms_logmnr.column_present(sql_redo_undo in raw,column_name in varchar2 default '') return number;
其中如果列在redo或undo部分存在，则返回1，否则返回0。

6）、dbms_logmnr_d.build
作用：用于建立字典文件
语法：dbms_logmnr_d.build(dictionary_filename in varcahr2,dictionary_location in varchar2,options in number);
其中dictionary_filename指定字段文件名，dictionary_location指定文件所在位置，options指定字典要写入位置(store_in_flat_file:文本文件,store_in_redo_log2:重新日志)

7）、dbms_logmnr_d.set_tablespace
作用：用于改变logminer表所在的表空间
语法：dbms_logmnr_d.set_tablespace(new_tablespace in default varchar2,dict_tablespace in default varchar2,spill_tablespace in default varchar2);
其中new_tablespace指定logminer表所在的表空间，dict_tablespace指定字典表所在表空间，spill_tablespace指定溢出表所在表空间。

回到顶部

3、综合例子
首先建表temp，然后执行dml操作和日志切换操作，生产归档日志。

复制代码
sqlplus /nolog
connect system/manager@test
create table temp(cola number,colb varchar2(10));
alter system swith logfile;
insert into temp values(9,'A');
update temp set cola=10;
commit;
alter system switch logfile;
delete from temp;
alter system switch logfile;
复制代码
 


1）、建立字典文件
说明：字典文件用于存放表及对象ID号之间的对应关系。从9i开始，字典信息既可被摘取到字段文件中，也可被摘取到重做日志中。摘取字典信息到字典文件方法如下：
（1）、设置字典文件所在目录
alter system set utl_file_dir="g:\test"
scope=spfile;
（2）、重启Oracle Server
sqlplus /nolog
conn sys/test@test as sysdba
shutdown immediate
startup
（3）、摘取字典信息
begin
dbms_logmnr_d.build(dictionary_filename=>'dict.ora',dictionary_location=>'g:\test\logminer');
end;

2）、建立日志分析列表
（1）、停止Oracle Server并装载数据库
sqlplus /nolog
conn sys/test@test as sysdba
shutdown immediate
startup mount
（2）、建立日志分析列表
begin
dbms_logmnr.add_logfile(options=>dbms_logmnr.new,logfilename=>'g:\test\arc1\test1.arc');
end;
（3）、增加其他日志文件(可选)
begin
dbms_logmnr.add_logfile(option=>dbms_logmnr.addfile,logfilename=>'g:\test\arc1\test12.arc');
end;

3）、启动LogMiner分析
begin
dbms_logmnr.start_logmnr(dictfilename=>'g:\test\logminer\dict.ora',
starttime=>to_date('2004-04-03:10:10:00','YYYY-MM-DD:HH24:MI:SS'),
endtime=>to_date('2004-04-03:15:30:00','YYYY-MM-DD:HH24:MI:SS'));
end;

4）、查看日志分析结果
说明：日志分析结果只能在当前会话查看。
（1）、显示DML分析结果
select operation,sql_redo,sql_undo from v$logmnr_contents where seg_name='TEMP';
（2）、显示DDL分析结果
select to_cahr(timestamp,'yyyy-mm-dd hh23:mi:ss') time,sql_redo from v$logmnr_contents where sql_redo like '%create%' or sql_redo like '%create%';
（3）、显示在用字典文件
select db_name,filename from v$logmnr_dictionary;

5）、结束LogMiner
execute dbms_logmnr.end_logmnr;


from:https://www.cnblogs.com/champaign/p/9468610.html