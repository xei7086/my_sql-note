SELECT 
	*	
FROM
	Table
where 1=1 
#SELECT TO_DAYS(‘1997-10-07′);  是从0年开始 到1997年10月7号之间的天数  结果就729669


#AND date_format(Cd_time,'%Y-%m-%d %H') > '2021-12-30 23'   #指定时间
#and to_days(Table.Cd_time) = to_days(now()); #今天
#and to_days(now())-to_days(Cd_time)<1  #昨天起
#前天
#and to_days(now())-to_days(Cd_time)<2 and to_days(now())-to_days(Cd_time)>1  

#本周
#and YEARWEEK(date_format(Cd_time,'%Y-%m-%d')) = YEARWEEK(now());

#上周
#and YEARWEEK(date_format(Cd_time,'%Y-%m-%d')) = YEARWEEK(now())-1;

#当前月
#and date_format(Cd_time,'%Y-%m')=date_format(now(),'%Y-%m')
#本月
#and DATE_FORMAT( Cd_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )

#距离当前6个月
#and Cd_time between date_sub(now(),interval 6 month) and now();